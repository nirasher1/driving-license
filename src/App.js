import React from 'react';
import DrivingLicense from "./components/drivingLicense/drivingLicense";
import './App.css';

function App() {
  return (
    <div className="page">
      <DrivingLicense />
    </div>
  );
}

export default App;
