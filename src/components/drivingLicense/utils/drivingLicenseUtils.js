export const stringToDate = (stateValue) => {
    let value = stateValue;
    if (value) {
        value = new Date(value);
    } else {
        value = null
    }
    return value
}

export const parseToDate = (value) => {
    if (value) {
        if (value instanceof Date) {
            return value
        } else {
            return stringToDate(value)
        }
    }
    return value
}