const validateNotEmpty = (value) => {
    return value !== "" ? null : "הערך לא יכול להיות ריק";
}

const validateNotContainNumbers = (value) => {
    return !(/\d/.test(value)) ? null : "הערך לא רשאי להכיל מספרים";
}

const validatePositiveInteger = (value) => {
    let n =  Math.floor(Number(value));
    return n !== Infinity && String(n) === String(Number(value)) && n > 0
        ? null : "הערך חייב להיות מספר חיובי שלם";
}

const validateNotNull = (value) => {
    return value !== null ? null : "תאריך לא תקני";
}

const validateDateInRange = (min = null, max = null, value) => {
    value.setHours(0, 0, 0, 0);
    if (min) {
        min.setHours(0, 0, 0, 0);
        if (!(value.getTime() >= min.getTime())) {
            return "תאריך ישן מידי";
        }
    }
    if (max) {
        max.setHours(0, 0, 0, 0);
        if (!(value.getTime() <= max.getTime())) {
            return "תאריך חדש מידי";
        }
    }
    return null;
}

const validateCharsLength = (min = null, max = null, value) => {
    if (min) {
        if (!(value.length >= min)) {
            return `הערך חייב לכלול ${min} תווים לכל הפחות`;
        }
    }
    if (max) {
        if (!(value.length <= max)) {
            return `הערך חייב לכלול ${max} תווים לכל היותר`;
        }
    }
    return null;
}

const runValidations = (validationsArray, value) => {
    for (let validationMethod of validationsArray) {
        const result = validationMethod(value);
        if (result) {
            return result
        }
    }
    return null;
}

export const validateString = (value) => {
    return runValidations([
        validateNotEmpty,
        validateNotContainNumbers
        ], value
    )
}

export const validateId = (value, minCharsNumber = null, maxCharsNumber = null) => {
    return runValidations([
        validateNotEmpty,
        validatePositiveInteger,
        () => validateCharsLength(minCharsNumber, maxCharsNumber, value)
        ], value
    )
}

export const validateDate = (value, min = null, max = null) => {
    return runValidations([
        validateNotEmpty,
        validateNotNull,
        (value) => validateDateInRange(min, max, value)
        ], value
    )
}

export const validateAddress = (value) => {
    return runValidations([
        validateNotEmpty,
        ], value
    )
}

export const validateLicenseType = (value) => {
    return runValidations([
        validateNotEmpty
        ], value
    )
}