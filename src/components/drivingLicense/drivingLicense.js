import React from 'react';
import Form from '../form/form'
import fieldsConfig from './fieldsConfig'
import './drivingLicense.css'


const DrivingLicense = () => {
    const handleSubmit = (fieldValues) => {
        alert(Object.keys(fieldValues).map(field => `${field}: ${fieldValues[field]}\r\n`))
    }

    return (
        <div className="driving-license-container">
            <Form className="driving-license"
                  fields={fieldsConfig}
                  onSubmit={handleSubmit}
            />
        </div>
    )
}

export default DrivingLicense;