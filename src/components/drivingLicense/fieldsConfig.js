import { validateString, validateDate, validateId, validateAddress, validateLicenseType } from './utils/validationUtils'
import { stringToDate, parseToDate } from "./utils/drivingLicenseUtils";


export default {
    lastName: {
        name: "lastName",
        type: "input",
        displayName: "שם משפחה",
        placeholder: "ישראלי",
        isValid: (value) => validateString(value),
        dependantFields: [],
    },
    firstName: {
        name: "firstName",
        type: "input",
        displayName: "שם פרטי",
        placeholder: "ישראל",
        isValid: (value) => validateString(value),
        dependantFields: [],
    },
    dateOfBirth: {
        name: "dateOfBirth",
        type: "date",
        displayName: "תאריך לידה",
        isValid: (value) => {
            value = parseToDate(value);
            let minDate = new Date();
            minDate.setFullYear(minDate.getFullYear() - 120)
            return validateDate(value, minDate, new Date())
        },
        dependantFields: ["activationDate", "expirationDate"],
    },
    activationDate: {
        name: "activationDate",
        type: "date",
        displayName: "תאריך תשלום",
        isValid: (value, prevFieldValues) => {
            value = parseToDate(value)
            let dateOfBirth = stringToDate(prevFieldValues.dateOfBirth);
            let minDate = dateOfBirth &&  new Date(dateOfBirth.setMonth(dateOfBirth.getMonth() + 16 * 12 + 9))
            return validateDate(value, minDate, new Date())
        },
        dependantFields: ["expirationDate"],
    },
    expirationDate: {
        name: "expirationDate",
        type: "date",
        displayName: "תוקף הרישיון",
        isValid: (value, prevFieldValues) => {
            let activationDate;
            value = parseToDate(value);
            activationDate = stringToDate(prevFieldValues.activationDate);
            let minDate = activationDate && new Date(activationDate.setMonth(activationDate.getMonth() + 2 * 12))
            activationDate = stringToDate(prevFieldValues.activationDate)
            let maxDate = activationDate && new Date(activationDate.setMonth(activationDate.getMonth() + 10 * 12))
            return validateDate(value, minDate, maxDate)
        },
        dependantFields: [],
    },
    personId: {
        name: "personId",
        type: "input",
        displayName: "מספר תעודת זהות",
        placeholder: "123456789",
        isValid: (value) => validateId(value, 9, 9),
        dependantFields: [],
    },
    licenseId: {
        name: "licenseId",
        type: "input",
        displayName: "מספר הרישיון",
        placeholder: "7654321",
        isValid: (value) => validateId(value, 7, 7),
        dependantFields: [],
    },
    address: {
        name: "address",
        type: "input",
        displayName: "המען",
        placeholder: "השושנים 132 תל-אביב",
        isValid: (value) => validateAddress(value),
        dependantFields: [],
    },
    licenseType: {
        name: "licenseType",
        type: "select",
        displayName: "קטגוריית כלי רכב",
        placeholder: "בחר ערך",
        isValid: (value) => validateLicenseType(value),
        dependantFields: [],
        options: {
            "A2": "אופנוע עד 14.6 כוחות סוס ועד 11 קילוואט",
            "A1": "אופנוע עד 47.46 כוחות סוס (35 קילוואט)",
            "A": "אופנוע שהספק מנועו עולה על 47.46 כוחות סוס (35 קילוואט)",
            "B": "רכב מנועי במשקל כולל מותר עד 3,500 ק\"ג, רשאי להסיע עד 8 נוסעים פרט לנהג, גם כאשר גורר נגרר במשקל עד 1500 ק\"ג",
            "C1": "רכב מנועי מסחרי ורכב עבודה במשקל כולל מותר מעל 3,500 ק\"ג ועד 12,000 ק\"ג, גם כאשר גורר נגרר במשקל כולל מותר עד 3,500 ק\"ג",
            "C": "רכב מנועי מסחרי ורכב עבודה במשקל כולל מותר מעל 12,000 ק\"ג, גם כאשר גורר נגרר במשקל כולל מותר עד 3,500 ק\"ג",
            "C+E": "רכב מנועי מסחרי ורכב עבודה במשקל כולל מותר מעל 3,500 ק\"ג, גם אם צמוד אליו נגרר או נתמך",
            "D": "אוטובוס",
            "D1": "מונית, רכב סיור ואוטובוס זעיר ציבורי במשקל כולל מותר עד 5,000 ק\"ג ומספר מקומות ישיבה עד 16 נוסעים מלבד הנהג",
            "D3": "אוטובוס זעיר פרטי",
            "1": "טרקטור",
        }
    },
}