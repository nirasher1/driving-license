import React from 'react';
import "./field.css"


const Field = (props) => {
    return (
        <div className="field-container">
            <div className="content-container">
                <div className="field-title">{props.displayName || "לא הוזנה כותרת"}:</div>
                {props.input}
            </div>
            <div className="message-container">
                {props.error &&
                <div className="error-message">{props.error}</div>
                }
            </div>
        </div>
    )
}

export default Field;