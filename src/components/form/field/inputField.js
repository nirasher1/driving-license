import React from 'react';
import Field from "./field";


const InputField = (props) => {
    const input = <input name={props.name}
                         type={props.type}
                         value={props.value}
                         placeholder={props.placeholder}
                         onChange={props.onChange}
    />;

    return (
        <Field {...props} input={input} />
    )
}

export default InputField;