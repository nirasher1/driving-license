import React from 'react';
import Field from "./field";
import './selectField.css'


const SelectField = (props) => {
    const select = (
        <select name={props.name}
                value={props.value}
                onChange={props.onChange}
        >
            <option hidden disabled value="" key="">{props.placeholder}</option>
            {Object.keys(props.options).map(option =>
                <option value={option} key={option} >
                    {`${option} - ${props.options[option]}`}
                </option>
            )}
        </select>
    );

    return (
        <Field {...props} input={select} />
    )
}

export default SelectField;