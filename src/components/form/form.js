import React from 'react';
import InputField from "./field/inputField";
import SelectField from "./field/selectField";


export default class Form extends React.Component {
    constructor(props) {
        super(props);
        this.initFieldValues();
    }

    initFieldValues = () => {
        const fieldValues = {};
        const errors = {};
        Object.keys(this.props.fields).forEach(fieldName => {
            fieldValues[fieldName] = "";
            errors[fieldName] = null;
        })
        // eslint-disable-next-line react/no-direct-mutation-state
        this.state = {
            fieldValues,
            errors
        }
    }

    setValue = (fieldName, newValue, isValidResult, dependantFields) => {
        this.setState((prevState) => ({
            fieldValues: {
                ...prevState.fieldValues,
                [fieldName]: newValue
            },
            errors: {
                ...prevState.errors,
                [fieldName]: isValidResult
            }
        }), () => {
            if (dependantFields && dependantFields.length) {
                const newErrors = {...this.state.errors}
                dependantFields.forEach(dependantFieldName => {
                    if (dependantFieldName !== fieldName && this.state.fieldValues[dependantFieldName]) {
                        newErrors[dependantFieldName] =
                            this.props.fields[dependantFieldName].isValid(
                                this.state.fieldValues[dependantFieldName].trim(),
                                this.state.fieldValues
                            )
                    }
                })
                this.setState({
                    errors: newErrors
                })
            }
        })
    }

    handleChange = (event, isValidFunc, dependantFields) => {
            const isValid = isValidFunc(
                event.target.valueAsDate || event.target.value.trim(),
                this.state.fieldValues
            );
            this.setValue(event.target.name, event.target.value, isValid, dependantFields)
    }

    generateField = (field) => {
        if (field.type === "input") {
            return <InputField {...field} key={field.name}
                               value={this.state.fieldValues[field.name]}
                               onChange={(event) => this.handleChange({...event}, field.isValid, field.dependantFields)}
                               error={this.state.errors[field.name]}
            />
        }
        if (field.type === "date") {
            return <InputField {...field} key={field.name}
                               value={this.state.fieldValues[field.name]}
                               onChange={(event) => this.handleChange({...event}, field.isValid, field.dependantFields)}
                               error={this.state.errors[field.name]}
            />
        }
        if (field.type === "select") {
            return <SelectField {...field} key={field.name}
                                value={this.state.fieldValues[field.name]}
                                onChange={(event) => this.handleChange({...event}, field.isValid, field.dependantFields)}
                                error={this.state.errors[field.name]}
            />
        }
        return null;
    }

    isFormCompleted = () => {
        return !Object.keys(this.state.fieldValues).some(field => !this.state.fieldValues[field])
            && !Object.keys(this.state.errors).some(field => this.state.errors[field]);
    }

    render = () => {
        return (
            <form className={`form-container ${this.props.className}`}
                  onSubmit={() => this.props.onSubmit(this.state.fieldValues)}
            >
                {this.state && Object.keys(this.props.fields).map((fieldName) =>
                    this.generateField(this.props.fields[fieldName]))}
                <input type="submit"
                       className="submit"
                       disabled={!this.isFormCompleted()}
                />
            </form>
        )
    }
}